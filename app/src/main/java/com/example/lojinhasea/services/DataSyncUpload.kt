package com.example.lojinhasea.services

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.lojinhasea.models.ProdutoModel
import io.realm.Realm
import com.example.lojinhasea.repository.api.Repository as ProductRepo

class DataSyncUpload(context: Context, workerParameters: WorkerParameters): Worker(context, workerParameters) {
    override fun doWork(): Result {

        createProduct()


        return Result.success()
    }

    // tenta criar o produto até 3 vezes
    private fun createProduct() {
        val list = Realm.getDefaultInstance().where(ProdutoModel::class.java)
            .findAll()

        for (product in list) {
            var count = 0
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!ProductRepo().create(product))
        }
    }
}