package com.example.lojinhasea.ui.produto

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lojinhasea.models.ProdutoModel
import com.example.lojinhasea.repository.bd.Repository as ProdutoDB
import com.example.lojinhasea.repository.api.Repository as ProdutoAPI

class ProdutoViewModel : ViewModel() {

    private val _produtoCheck = MutableLiveData<Boolean>().apply {
        value = true
    }
    val produtoCheck: LiveData<Boolean> = _produtoCheck

    //iniciando as variaveis que recebe a lista com o modelo do produto
    private val _produtos = MutableLiveData<ArrayList<ProdutoModel>>().apply { value = ArrayList() }
    val produtos: LiveData<ArrayList<ProdutoModel>> = _produtos


    //função para criar o produto no repositorio
    fun createProduct(produto: ProdutoModel) {
        ProdutoDB().create(produto)
    }

    fun getAllProductApi() {
        return ProdutoAPI().get(_produtoCheck)
    }

    //chamando a função para pegar o produto do repositorio
    fun getAllProduct() {
        ProdutoDB().getAll(_produtos)
    }

    //função para vender o produto e alterar os dados no repositorio
    fun saleProduct(qttStock: Int, qttSale: Int, item: Int){
        var obj = _produtos.value?.get(item)!!
        ProdutoDB().saleProduct(qttStock, qttSale, _produtos, obj)
        getAllProduct()
    }



}