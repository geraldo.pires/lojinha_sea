package com.example.lojinhasea.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.lojinhasea.databinding.TelaHomeUI


class TelaHome : Fragment() {

    lateinit var binding: TelaHomeUI

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = TelaHomeUI.inflate(inflater)

        //botão de abrir lista de produtos
        binding.buttonListProduct.setOnClickListener {
            var action = TelaHomeDirections.actionTelaHomeToListaProduto()
            findNavController().navigate(action)
        }
        return binding.root
    }

}