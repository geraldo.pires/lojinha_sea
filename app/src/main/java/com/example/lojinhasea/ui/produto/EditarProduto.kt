package com.example.lojinhasea.ui.produto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.lojinhasea.databinding.CadastroProdutoUI
import com.example.lojinhasea.models.ProdutoModel

class EditarProduto: Fragment()  {
    private lateinit var mBinding: CadastroProdutoUI
    private lateinit var viewModel: ProdutoViewModel
    lateinit var Product: ProdutoModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = CadastroProdutoUI.inflate(inflater)
        mBinding.textTitle.text = "Alterar Produto"
        viewModel = ViewModelProvider(requireActivity()).get(ProdutoViewModel::class.java)

        //save product
        mBinding.buttonSave.setOnClickListener {
           // saveProduct()
        }
        //return to previous page
        mBinding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }
        return mBinding.root
    }
}