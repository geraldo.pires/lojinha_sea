package com.example.lojinhasea.ui.produto

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.lojinhasea.databinding.CadastroProdutoUI
import com.example.lojinhasea.models.ProdutoModel
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CadastroProduto : Fragment() {

    //iniciando as variaveis privadas de binding e viewmodel
    private lateinit var mBinding: CadastroProdutoUI
    private lateinit var viewModel: ProdutoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflando o layout de cadastro de produto no fragmento
        mBinding = CadastroProdutoUI.inflate(inflater)

        //vinculando a viewmodel do produto
        viewModel = ViewModelProvider(requireActivity()).get(ProdutoViewModel::class.java)

        //botão de salvar produto
        mBinding.buttonSave.setOnClickListener {

            saveProduct()

        }
        //retornar para pagina anterior
        mBinding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }


        return mBinding.root
    }

    //função para salvar o produto
    private fun saveProduct() {

        try {
            //Salvando as entradas de dados dos usuários nas variáveis
            val name = mBinding.productName.text.toString()
            val description = mBinding.productDescription.text.toString()
            val category = mBinding.spinnerStatic.selectedItem.toString()
            val price = mBinding.productPrice.text.toString().toFloat()
            val qttStock = mBinding.productQttStock.text.toString().toInt()

            //passando a classe ProdutoModel como valor na variavel product
            val product = ProdutoModel()

            //passando as variaveis que possui as informações do produto para os atributos da ProdutoModel
            product.name = name
            product.description = description
            product.category = category
            product.price = price
            product.qttStock = qttStock

            //criando o produto com base nas informações do usuário
            viewModel.createProduct(product)

            //após salvar retorne para página antetior
            findNavController().popBackStack()
            //msg de adicionado com sucesso
            Snackbar.make(
                mBinding.root,
                "Produto adicionado com sucesso!", Snackbar.LENGTH_SHORT
            ).show()

        } // msg de erro
        catch (nfe: NumberFormatException) {
            //in case of error in the values call a snackbar
            Snackbar.make(
                mBinding.root,
                "Informe valores validos", Snackbar.LENGTH_SHORT
            ).show()
        }

    }


}

//////Demonstração do spinner dinamico
/*

//XML
<Spinner
android:id="@+id/spinner_dynamic"
android:layout_width="match_parent"
android:layout_height="wrap_content"
android:layout_marginTop="16dp"/>

//chamada
loadSpinner()

//View
private fun loadSpinner() {
        //lista de strings, essa lista poderia vim de um banco de dados de cadastro de categoria tambem
        val mList = listOf("Alimentos", "Bebidas", "Limpeza", "Frutas", "Higiene")

        val adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item, mList)

        spinner_dynamic.adapter = adapter
}

 */

