package com.example.lojinhasea.ui.produto

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View
import android.widget.PopupMenu
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.*
import com.example.lojinhasea.R
import com.example.lojinhasea.databinding.ItemProductUI
import com.example.lojinhasea.databinding.ListaProdutoUI
import com.example.lojinhasea.models.ProdutoModel
import com.example.lojinhasea.services.DataSyncDownload
import com.example.lojinhasea.services.DataSyncUpload
import java.time.Duration
import java.util.concurrent.TimeUnit


class ListaProduto : Fragment() {

    private lateinit var binding: ListaProdutoUI
    private lateinit var viewModel: ProdutoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = ListaProdutoUI.inflate(inflater)
        viewModel = ViewModelProvider(requireActivity()).get(ProdutoViewModel::class.java)


        apiRequest()



        //pegando a lista de produtos
        viewModel.getAllProduct()
        viewModel.getAllProductApi()

        //observando a lista de produtos
        viewModel.produtos.observe(viewLifecycleOwner, Observer {
            binding.recyclerview.adapter = ProductAdapter(it)
            binding.recyclerview.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        })



        //botão para adicionar um novo produto
        binding.addProduct.setOnClickListener {
            findNavController().navigate(ListaProdutoDirections.actionListaProdutoToCadastroProduto())
        }
        return binding.root
    }

    //Adapter e viewHolder do recyclerView da Lista de produtos
    inner class ProductAdapter(val list: ArrayList<ProdutoModel>) :
        RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
        inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var bind: ItemProductUI

            init {
                bind = ItemProductUI.bind(itemView)
            }
        }



        //configurando o layout
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
            return ProductViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_aluno, parent, false)
            )
        }

        @SuppressLint("ResourceAsColor")
        override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
            val product = list[position]
            //alterando os valores dos items da lista
            holder.bind.fieldName.text = product.name
            holder.bind.category.text = product.category

            holder.bind.buttonAction.setOnClickListener() {
                val popup = PopupMenu(requireContext(), holder.bind.buttonAction)
                popup.inflate(R.menu.menu_file)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.alterProduct -> {
                            findNavController().navigate(
                                ListaProdutoDirections.actionListaProdutoToEditarProduto()
                            )

                            true
                        }
                        R.id.deleteProduct -> true
                        else -> false
                    }
                }
                popup.show()
            }

            val hiddenInfo = holder.bind.moreInfo

            //ação ao clica em cima de um item da lista
            holder.bind.relative1.setOnClickListener {

                //se a lista estiver visivel, torne-a invisivel
                if (hiddenInfo.isVisible) {
                    hiddenInfo.visibility = View.GONE
                }
                //se a lista estiver invisivel, torne-a visivel
                else {
                    hiddenInfo.visibility = View.VISIBLE

                    //alterando os valores dos produtos na lista
                    holder.bind.description.text = product.description

                    var stock = holder.bind.stockValue.text.toString().toInt()

                    holder.bind.price.text = product.price.toString()
                    holder.bind.saleValue.text = product.qttSale.toString()

                    holder.bind.stockValue.text = product.qttStock.toString()

                    //função do botão para chamar o metodo de compra da viewmodel
                    holder.bind.buttonBuy.setOnClickListener {
                        //pegando a quantidade de produto desejada
                        var qttSale = holder.bind.qtdSale.text.toString().toInt()
                        viewModel.saleProduct(stock, qttSale, position)
                    }

                }

            }

        }
        //pegando a quantidade de items na lista
        override fun getItemCount(): Int {
            return list.size
        }

    }

    fun apiRequest(){
        try {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val upload = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                PeriodicWorkRequestBuilder<DataSyncUpload>(
                    900000,
                    TimeUnit.MILLISECONDS
                )
                    .setConstraints(constraints)
                    .setInitialDelay(Duration.ofMillis(100L))
                    .addTag("UPLOAD")
                    .build()

            } else {
                PeriodicWorkRequestBuilder<DataSyncUpload>(
                    900000,
                    TimeUnit.MILLISECONDS
                )
                    .setConstraints(constraints)
                    .addTag("UPLOAD")
                    .setInitialDelay(100, TimeUnit.MILLISECONDS)
                    .build()
            }
            val download = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                PeriodicWorkRequestBuilder<DataSyncDownload>(
                    900000,
                    TimeUnit.MILLISECONDS
                )
                    .addTag("DOWNLOAD")
                    .setInitialDelay(Duration.ofMillis(100L))
                    .build()
            } else {
                PeriodicWorkRequestBuilder<DataSyncDownload>(
                    900000,
                    TimeUnit.MILLISECONDS
                )
                    .addTag("DOWNLOAD")
                    .setInitialDelay(100, TimeUnit.MILLISECONDS)
                    .build()
            }


            WorkManager.getInstance(requireContext()).enqueueUniquePeriodicWork(
                "UPLOAD",
                ExistingPeriodicWorkPolicy.REPLACE, upload
            )

            WorkManager.getInstance(requireContext()).enqueueUniquePeriodicWork(
                "DOWNLOAD",
                ExistingPeriodicWorkPolicy.REPLACE, download
            )
        } catch (e: NullPointerException) {
            WorkManager.getInstance(requireContext()).cancelAllWork()

        }
    }

    }

inline fun <reified W : ListenableWorker> PeriodicWorkRequestBuilder(
    repeatInterval: Long,
    repeatIntervalTimeUnit: TimeUnit
): PeriodicWorkRequest.Builder {
    return PeriodicWorkRequest.Builder(W::class.java, repeatInterval, repeatIntervalTimeUnit)
}

