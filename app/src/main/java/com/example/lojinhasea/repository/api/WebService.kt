package com.example.lojinhasea.repository.api

import com.example.lojinhasea.models.ProdutoModel
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ProductService{
    @GET("products")
    fun get(): Call<ArrayList<ProdutoModel>>

    @POST("products")
    fun create(
        @Body body: JsonObject,
    ):  Call<ProdutoModel>
}