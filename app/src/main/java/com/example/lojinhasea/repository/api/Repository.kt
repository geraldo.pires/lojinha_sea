package com.example.lojinhasea.repository.api

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.lojinhasea.models.ProdutoModel
import com.google.android.material.snackbar.Snackbar
import io.realm.Realm
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalStateException

class Repository() {
    val baseUrl = "http://172.18.190.4:8000/"
    val httpClient = OkHttpClient.Builder()

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val webService: ProductService = retrofit.create(
        ProductService::class.java
    )

    fun create(product: ProdutoModel): Boolean{
        val json = product.toJson()
        Log.e("PRODUCT_JSON: ",product.toJson().toString())
        val response = webService.create(
            body = json
        ).execute()

        if (response.isSuccessful){
            try {
                val realm = Realm.getDefaultInstance()
                realm.executeTransaction {
                    val productQuery = realm.where(ProdutoModel::class.java)
                        .equalTo("id", product.id).findFirst()

                }
                realm.close()
                Log.e("Create-successful: ", "Json "+response.body())
                Log.e("Create-successful: ", "Code "+response.code())
                return true
            } catch (e: IllegalStateException){
                Log.e("Create()-error: ",e.toString())
                return false
            }
        } else {

            val text = response.errorBody()?.string()
            return false
        }
    }

    fun get (productCheck: MutableLiveData<Boolean>){

        webService.get()
            .enqueue(object : Callback<ArrayList<ProdutoModel>> {
                override fun onResponse(
                    call: Call<ArrayList<ProdutoModel>>,
                    response: Response<ArrayList<ProdutoModel>>
                ) {
                    if (response.isSuccessful){
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction{
                            realm.insertOrUpdate(response.body())
                            Log.e("Get-successful: ", "get funcionando")
                            Log.e("Get-successful: ", response.body()?.size.toString())
                        }
                    }

                    productCheck.value = false
                }

                override fun onFailure(call: Call<ArrayList<ProdutoModel>>, t: Throwable) {
                    val a = t
                    productCheck.value = false
                    Log.e("Get()-error: ",a.toString())
                }
            })
}

}