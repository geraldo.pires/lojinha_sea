package com.example.lojinhasea.repository.bd

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.lojinhasea.models.ProdutoModel
import com.example.lojinhasea.repository.api.ProductService
import com.example.lojinhasea.repository.api.RetrofitClient
import io.realm.Realm
import io.realm.kotlin.where
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository {

    //criando e inserindo o produto no banco
    fun create(produto: ProdutoModel){
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction {
            it.insert(produto)
        }
    }

    fun get (idProdutoModel: String): ProdutoModel{
        val realm = Realm.getDefaultInstance()
        val project = realm.where<ProdutoModel>()
            .equalTo("id", idProdutoModel).findFirst()
        project?.let { aux ->
            return aux
        }?: run{
            return ProdutoModel()
        }

        /*webService.get()
            .enqueue(object : Callback<ArrayList<ProdutoModel>>{
                override fun onResponse(
                    call: Call<ArrayList<ProdutoModel>>,
                    response: Response<ArrayList<ProdutoModel>>
                ) {
                    if (response.isSuccessful){
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction{
                            realm.insertOrUpdate(response.body())
                           // Log.e("Get(leads)-successful: ", "uhuuuu")
                        }
                    }

                    productCheck.value = false
                }

                override fun onFailure(call: Call<ArrayList<ProdutoModel>>, t: Throwable) {
                    val a = t
                    productCheck.value = false
                    Log.e("Get()-error: ",a.toString())
                }
            })*/

        /*val remote = RetrofitClient.createService((ProductService::class.java))
        val call: Call<List<ProdutoModel>> = remote.list()*/

    }

    //pegando os dados do ProdutoModel e guardando no repositorio
    fun getAll(_produtos: MutableLiveData<ArrayList<ProdutoModel>>){
        val realm = Realm.getDefaultInstance()

        realm.addChangeListener {

            val produtos = realm.where<ProdutoModel>().findAll()
            var list = ArrayList<ProdutoModel>()

            list.addAll(produtos)
            list = ArrayList<ProdutoModel>()
            list.addAll(produtos)
            _produtos.postValue(list)

            _produtos.value = list
        }
    }

    // função com a lógica de venda
    fun saleProduct(
        qttStock: Int,
        qttSale: Int,
        _produtos: MutableLiveData<ArrayList<ProdutoModel>>,
        obj: ProdutoModel
    ) {
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction {
            obj.qttSale += qttSale
            obj.qttStock = (obj.qttStock - qttSale)
        }

        getAll(_produtos)
    }

    fun alterProduct(produtos: MutableLiveData<ArrayList<ProdutoModel>>, obj: ProdutoModel){
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction{
            // continuar daqui
        }
    }

}

