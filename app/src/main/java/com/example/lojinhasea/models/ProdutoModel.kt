package com.example.lojinhasea.models

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId
import java.util.*

//modelo do objeto Produto com seus atributos
open class ProdutoModel(): RealmObject() {

    @PrimaryKey @SerializedName("_id")
    var id: String = ObjectId().toString()
    var name: String = ""
    var description: String = ""
    var category: String = ""
    var price: Float = 0.0f
    var qttStock: Int = 0
    var qttSale: Int = 0
    var created: Long = Calendar.getInstance().timeInMillis

    fun toJson(): JsonObject{
        val json = JsonObject()
        json.addProperty("pk", id)
        json.addProperty("name", name)
        json.addProperty("description", description)
        json.addProperty("category", category)
        json.addProperty("quantity_sold", qttSale)
        json.addProperty("quantity_stock", qttStock)
        json.addProperty("crated", created)
        return json

    }

}

